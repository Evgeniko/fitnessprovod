<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Instructor;
use Faker\Generator as Faker;

$factory->define(Instructor::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'city_id' => rand(1, 10),
        'course_id' => rand(1, 10)
    ];
});
