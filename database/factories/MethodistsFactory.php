<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Methodist;
use Faker\Generator as Faker;

$factory->define(Methodist::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'city_id' => rand(1, 10),
        'vk_link' => $faker->url,
        'inst_link' => $faker->url,
        'fb_link' => $faker->url,
        'info' => $faker->realText()
    ];
});
