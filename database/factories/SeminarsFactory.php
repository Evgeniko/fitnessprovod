<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Seminar;
use Faker\Generator as Faker;

$factory->define(Seminar::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'city_id' => rand(1, 10),
        'course_id' => rand(1, 10),
        'price' => rand(1000, 10000),
        'methodist_id' => rand(1, 10),
        'date_start' => $faker->date('Y-m-d'),
        'date_end' => date('Y-m-d', 1561915687),
    ];
});
