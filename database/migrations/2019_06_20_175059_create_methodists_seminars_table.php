<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMethodistsSeminarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('methodists_seminars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('methodist_id')->unsigned();
            $table->bigInteger('seminar_id')->unsigned();
            $table->timestamps();

            $table->foreign('methodist_id')->references('id')->on('methodists');
            $table->foreign('seminar_id')->references('id')->on('seminars');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('methodists_seminars');
    }
}
