<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeminarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seminars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('city_id')->unsigned();
            $table->bigInteger('course_id')->unsigned();
            $table->bigInteger('methodist_id')->unsigned();
            $table->bigInteger('price');
            $table->date('date_start');
            $table->date('date_end');
            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('course_id')->references('id')->on('courses');
            $table->foreign('methodist_id')->references('id')->on('methodists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seminars');
    }
}
