<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Разработчик',
            'email' => 'admin@admin.ru',
            'password' => bcrypt('123321')
        ]);
    }
}
