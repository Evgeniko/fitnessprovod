<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
             UsersTableSeeder::class,
             CoursesTableSeeder::class,
             CitiesTableSeeder::class,
             MethodistsTableSeeder::class,
             SeminarsTableSeeder::class,
             InstructorsTableSeeder::class
         ]);

         for($i = 1; $i <= 10; $i++){
             DB::table('instructors_courses')->insert([
                 'instructor_id' => $i,
                 'course_id' => rand(1,10)
             ]);
             DB::table('methodists_seminars')->insert([
                 'methodist_id' => $i,
                 'seminar_id' => rand(1,10)
             ]);
         }

         DB::table('cart')->insert([
             'user_id' => 1,
             'seminar_id' => 3
         ]);


         /* demomod */
        DB::table('demomod')->insert([
            'now' => now()
        ]);
        /*demomod*/

        for($i = 1; $i <= 10; $i++) {
            DB::table('purchases')->insert([
                'seminar_id' => $i,
                'user_id' => 1,
            ]);
        }
    }
}
