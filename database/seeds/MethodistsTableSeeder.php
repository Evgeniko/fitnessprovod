<?php

use Illuminate\Database\Seeder;

class MethodistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Methodist::class, 10)->create()->each(function ($methodist){});
    }
}
