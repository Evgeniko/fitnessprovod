<?php

use App\Models\Instructor;
use Illuminate\Database\Seeder;

class InstructorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Instructor::class, 10)->create()->each(function ($instructor){});
    }
}
