<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('page.main');

Route::get('/seminars', 'SeminarsController@index')->name('seminars');
Route::get('/courses', 'CoursesController@index')->name('courses');
Route::get('/methodists', 'MethodistsController@index')->name('methodists');
Route::get('/methodist/{id}', 'MethodistsController@showById')->name('methodist.view');
Route::get('/instructors', 'InstructorsController@index')->name('instructors');
Route::get('/contacts', 'ContactsController@index')->name('contacts');

Auth::routes();


Route::middleware('auth')->group(function (){
    Route::get('/profile', 'ProfileController@subscriptionsShow')->name('profile.subscriptions');
    Route::get('/profile/settings', 'ProfileController@settingsShow')->name('profile.settings');
    Route::get('/profile/purchases', 'ProfileController@purchasesShow')->name('profile.purchases');

    Route::get('/cart', 'CartController@index')->name('cart');
    Route::post('/cart/add/{id}', 'CartController@add')->name('cart.add');

    Route::post('/pay', 'PaymentController@pay')->name('pay');
});


/**demomod*/

Route::get('/purchases/visited/{id}', 'DemoModController@visitPurchase')->name('demo.visited');
Route::post('/current/date', 'DemoModController@changeCurrentDate')->name('change.date');