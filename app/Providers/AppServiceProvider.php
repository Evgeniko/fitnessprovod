<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Директива определяющая есть ли уже в корзиене пользователя отображаемый товар
        Blade::if('hasSeminarInCart', function ($id) {
            return Auth::user()->seminarsInCart->contains(function ($item) use ($id) {
                if ($item->id == $id) {
                    //dd($item->id, $seminar->id);
                    return $id;
                }else{
                    return false;
                }
            });
        });
    }
}
