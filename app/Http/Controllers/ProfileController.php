<?php

namespace App\Http\Controllers;

use App\Models\Purchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function subscriptionsShow()
    {
        $subscriptions =  Auth::user()->subscriptions()->with(['seminar.course', 'seminar.city'])->get();
        $possibleSubscriptions = Auth::user()->purchases()->visited()->get();
        return view('profile.mysubscriptions', compact('subscriptions', 'possibleSubscriptions'));
    }

    public function settingsShow()
    {
        return view('profile.mysettings');
    }

    public function purchasesShow()
    {
        $purchases = Auth::user()->purchases()->with('seminar.methodist')->get();
        return view('profile.mypurchases', compact('purchases'));
    }
}
