<?php

namespace App\Http\Controllers;

use App\Models\Methodist;
use Illuminate\Http\Request;

class MethodistsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $methodists = Methodist::with('city')->get();
        return view('methodists.index', compact('methodists'));
    }


    public function showById($id)
    {
        $methodist = Methodist::find($id);
        return view('methodists.one', compact('methodist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Methodist  $methodist
     * @return \Illuminate\Http\Response
     */
    public function show(Methodist $methodist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Methodist  $methodist
     * @return \Illuminate\Http\Response
     */
    public function edit(Methodist $methodist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Methodist  $methodist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Methodist $methodist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Methodist  $methodist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Methodist $methodist)
    {
        //
    }
}
