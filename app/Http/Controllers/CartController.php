<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{

    /**
     * Добавление продукта в корзину
     *
     * @param Request $request
     * @param $id
     * @return bool
     */
    public function add(Request $request, $id)
    {
        if ($this->checkInCart(Auth::id(), $id)){

            $result = Cart::create(['user_id' => Auth::id(), 'seminar_id' => $id]);

            //return $result;
        }

        //return false;

        return redirect('cart');
    }

    public function remove(Request $request, $id)
    {
        Cart::where(['user_id' => Auth::id(),'seminar_id' => $id])->delete();

        return redirect('cart');
    }

    /**
     * Проверка на присутствие товара в корзине пользователя
     * @param $userId
     * @param $productId
     * @return bool
     */
    private function checkInCart($userId, $productId)
    {
        return !!Cart::where(['user_id' => $userId, 'seminar_id' => $productId])->get();
    }


    /**
     * Просмотр товаров пользователя в корзине
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Auth::user()->seminarsInCart;
        return view('cart.index', compact('products'));
    }
}
