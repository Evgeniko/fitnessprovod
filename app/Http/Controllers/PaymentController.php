<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Purchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    public function pay()
    {
        $user = Auth::user();
        $semainars = $user->seminarsInCart;
        DB::transaction(function () use($semainars, $user) {
            foreach ($semainars as $semainar){
                Cart::where(['user_id' => $user->id, 'seminar_id' => $semainar->id])->delete();
                Purchase::create(['user_id' => $user->id, 'seminar_id' => $semainar->id]);
            }
        });
        redirect('profile.purchases');
    }
}
