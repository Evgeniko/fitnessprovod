<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seminar extends Model
{

    protected $dates = ['date_start', 'date_end'];

    //protected $dateFormat = 'd.m.Y';

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function course()
    {
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }

    public function methodist()
    {
        return $this->belongsTo(Methodist::class, 'methodist_id', 'id');
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class, 'seminar_id', 'id');
    }

    public function scopeAvailable($query)
    {
        /**demomod*/
            $now = DemoMod::find(1)->now;
            //dd($now);
        /**demomod*/

        return $query->where('date_end', '>=', $now);
    }
}
