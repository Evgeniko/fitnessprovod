<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function seminars()
    {
        return $this->hasMany(Seminar::class, 'seminar_id', 'id');
    }
}
