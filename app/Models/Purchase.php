<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $table = 'purchases';

    protected $guarded = ['id'];

    public function seminar()
    {
        return $this->belongsTo(Seminar::class, 'seminar_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    public function scopeVisited($query)
    {
        return $query->where('visited', true);
    }
}
