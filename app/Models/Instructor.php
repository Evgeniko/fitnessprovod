<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instructor extends Model
{
    public function city()
    {
        return $this->hasOne('App\Models\City', 'id', 'city_id');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Models\Course', 'instructors_courses', 'instructor_id', 'course_id');
    }
}
