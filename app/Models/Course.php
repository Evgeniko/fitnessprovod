<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';

    public function seminars()
    {
        return $this->hasMany(Seminar::class, 'course_id', 'id');
    }
}
