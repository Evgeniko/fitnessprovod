<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Methodist extends Model
{
    public function city()
    {
        return $this->hasOne('App\Models\City', 'id', 'city_id');
    }

    public function seminars()
    {
        return $this->hasMany(Seminar::class, 'seminar_id', 'id');
    }
}
