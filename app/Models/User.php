<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function seminarsInCart()
    {
        return $this->belongsToMany(Seminar::class, 'cart', 'user_id', 'seminar_id');
    }


    public function purchases()
    {
        return $this->hasMany(Purchase::class, 'user_id', 'id');
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscribe::class, 'user_id', 'id');
    }
}
