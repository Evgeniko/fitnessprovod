@extends('layouts.app')
@section('content')
    <section class="authorize">
        <div class="wrapper" style='text-align: left;'>
            <hr>
            <p class="breadcrumbs">
                <a href="{{ route('page.main') }}">Главная</a> / Релизы				</p><div class='nc_list nc_text'>
                <div class='nc_row'>
                    <p>Минимум подготовки к уроку и максимум результата!&nbsp;<br />
                        Думаешь, невозможно?&nbsp;<br />
                        Возможно с нами, ведь мы продумали все до мелочей!&nbsp;</p>
                    <p>Твоя задача &ndash; просто вести тренировку по подготовленному материалу и совершенствовать свою технику и тренерские навыки!&nbsp;<br />
                        Тебе больше не нужно ломать голову над хореографией и заниматься поиском музыки!&nbsp;<br />
                        Ты больше не будешь уставать от одного и того же, от повторяющихся изо дня в день, одинаковых занятий.&nbsp;</p>
                    <p>Наш опыт позволяет нам &laquo;работать за тебя&raquo;, пока ты &laquo;работаешь на себя&raquo;.&nbsp;</p>        </div>
            </div>

        </div><br/><br/>





        <div id="casing" class="module_index">
            <div id="content" style="padding:30px 0 60px 0">
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="progs-anno">

                        <iframe src="https://player.vimeo.com/video/342929663" width="660" height="350" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

                    </div>
                </div>




                <div class="col-md-7 col-sm-6 col-xs-12 progs-descr">
                    <span class="progs-title"><img src="{{ asset('images/system/5385_4.png') }}" height="80"></span>
                    <br>




                    <hr>
                    <p><p style="text-align: justify;">Групповой урок &laquo;Stretching&raquo; может быть основан на статических упражнениях, или на динамических, а можно это совместить. Комфортные переходы из одного исходного положения в другое, необычные варианты соединения обычных упражнений, и как следствие, сбалансированный тренировочный процесс для развития гибкости. Комфортно и эффективно для клиентов с любом уровнем подготовленности.</p></p>


                </div>



                <div class="clr" style="margin-bottom: 50px"></div>





            </div>
        </div>







        <div id="casing" class="module_index">
            <div id="content" style="padding:30px 0 60px 0">
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="progs-anno">

                        <script type="text/javascript" src="../../s3.spruto.org/embed/player.js"></script>
                        <script class="splayer"> var params = {"playlist":[{"video":[{"url":"https://fitnessprovod.com/netcat_files/5387_3.mp4"}],"duration":0,"posterUrl":"/netcat_files/5386_3.png"}],"uiLanguage":"ru","width":"100%","height":"350"}; player.embed(params); </script>



                        <!----------a href="/relizy/video_3.html">	<img src="/netcat_files/5388_3.png"> </a---------->

                    </div>
                </div>




                <div class="col-md-7 col-sm-6 col-xs-12 progs-descr">
                    <span class="progs-title"><img src="{{ asset('images/system/5385_3.png') }}" height="80"></span>
                    <br>




                    <hr>
                    <p><p style="text-align: justify;">Классическая степ-аэробика, в полном смысле слова &laquo;классическая&raquo;. Простая, интересная, без сложных танцевальных элементов и максимально адаптированная под начинающих клиентов фитнес-клубов и студий.</p></p>


                </div>



                <div class="clr" style="margin-bottom: 50px"></div>





            </div>
        </div>







        <div id="casing" class="module_index">
            <div id="content" style="padding:30px 0 60px 0">
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="progs-anno">

                        <iframe src="https://player.vimeo.com/video/342929645" width="660" height="350" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

                    </div>
                </div>




                <div class="col-md-7 col-sm-6 col-xs-12 progs-descr">
                    <span class="progs-title"><img src="{{ asset('images/system/5385_1.png') }}" height="80"></span>
                    <br>




                    <hr>
                    <p><p style="text-align: justify;">В условиях отсутствия степ-платформы, гантелей и прочего оборудования, а также при наличии всего этого, урок с весом собственного тела разовьет общую выносливость, силовую выносливость, ловкость, быстроту и гибкость ваших клиентов. Ограничения в залах и оборудовании больше нет, есть функциональная тренировка, которую можно провести даже на улице. Плюс четкая система периодизации тренировочного процесса &ndash; для мотивации и удержания ваших клиентов.</p></p>


                </div>



                <div class="clr" style="margin-bottom: 50px"></div>





            </div>
        </div>

        </div>
    </section>
@endsection