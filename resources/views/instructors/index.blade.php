@extends('layouts.app')
@section('content')
    <section class="authorize">
    <div class="wrapper">
        <hr>
        <p class="breadcrumbs">
            <a href="/">Главная</a> / Инструкторы
        </p>
        <div id="casing" class="module_index">
            <div id="content">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-3 col-sm-12 col-xs-12 schedule-block">
                        <p>Инструкторы</p>
                    </div>
                    <div class="col-md-4 col-sm-5 col-xs-5 schedule-block1">

                        <select name="f4" class="data-filter" onchange="location='?prog='+this.value;">
                            <option selected value="0">Все программы</option>
                        </select>
                    </div>
                    <div class="col-md-4 col-sm-5 col-xs-5 schedule-block1">
                        <select name="f5" class="data-filter" onchange="location='?city='+this.value;">
                            <option selected value="">Все города</option>
                        </select>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-2 schedule-block1">
                        <a href="#" onclick="reset()">Сбросить</a>
                    </div>
                    <div class="clr"></div>
                    @foreach ($instructors as $instructor)
                        <div class="my-orders" data-order="{{ $instructor->id }}">

                            <div class="col-md-6 col-sm-4 col-xs-6 order-data">
                                <p class="order-name" style='font-size:18px;'>{{ $instructor->name }}</p>
                                <p class="order-city">{{ $instructor->city->name }}</p>
                                <br/>
                            </div>

                            <div class="col-md-6 col-sm-4 col-xs-6 order-data">
                                <p class="order-name" style='font-size:20px;'>
                                    @foreach($instructor->courses as $course)
                                        {{ $course->name }}
                                    @endforeach
                                </p>
                            </div>

                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection