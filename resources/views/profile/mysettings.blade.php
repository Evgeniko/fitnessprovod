@extends('layouts.app')
@section('content')
    <section class="authorize">
        <div class="wrapper">
            <hr>
            <p class="breadcrumbs">
                <a href="/">Главная</a> / Личный кабинет
            </p>
            <div id="casing" class="module_index">
                <div id="content">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="clr" style="padding: 15px 0"></div><form name="adminForm" id="adminForm" class="nc-form" enctype="multipart/form-data" method="post" action="/netcat/message.php">
                            <div id="nc_moderate_form">
                                <div class="nc_clear"></div>
                                <input name="admin_mode" type="hidden" value="">
                                <input type="hidden" name="nc_token" value="80465a47d621800dbe039b4d62cda39c"><input name="catalogue" type="hidden" value="15">
                                <input name="cc" type="hidden" value="748">
                                <input name="sub" type="hidden" value="802"><input name="message" type="hidden" value="494">
                                <input name="posting" type="hidden" value="1">
                                <input name="curPos" type="hidden" value="0">
                                <input name="f_Parent_Message_ID" type="hidden" value="">
                                <input type="hidden" name="f_Checked" value="1"></div>
                            <div class="reg-form">
                                <table class="reg-table">
                                    <tbody>
                                    <tr>
                                        <td style="width:25%">Имя</td>
                                        <td>
                                            <div align="center" class="schQuery">
                                                <input type="text" name="f_ForumName" value="Сергей" maxlength="50" size="20" class="queryField">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Фамилия</td>
                                        <td>
                                            <div align="center" class="schQuery">
                                                <input type="text" name="f_Surname" value="Барилко" maxlength="50" size="20" class="queryField">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>E-mail</td>
                                        <td>
                                            <div align="center" class="schQuery">
                                                <input type="text" name="f_Email" value="barilkosa@gmail.com" maxlength="50" size="20" class="queryField">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Телефон</td>
                                        <td>
                                            <div align="center" class="schQuery">
                                                <input type="text" name="f_Phone" value="+79635455503" id="phone1" maxlength="50" size="20" class="queryField">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Город</td>
                                        <td>
                                            <div align="center" class="schQuery">
                                                <input type="text" name="f_City" value="Ижевск" maxlength="50" size="20" class="queryField">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="no-after">&nbsp;</td>
                                        <td>
                                            <div align="left" class="schQuery">
                                                <label><input type="checkbox" name="checkbox1" class="customCheckbox" value="checkbox" <input="" style="display: none;"><span class="customCheckbox" title=""></span>Хочу получать информацию о новых релизах и семинарах</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="no-after">&nbsp;</td>
                                        <td>
                                            <div align="left" class="schQuery">
                                                <label><input type="checkbox" name="checkbox2" class="customCheckbox" value="checkbox" checked="" required="" style="display: none;"><span class="customCheckbox" title=""></span>Я соглашаюсь с условиями <a href="/dogovor-publichnoy-oferty/">Публичной оферты</a></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="no-after" colspan="2">
                                            <div align="center" class="schBtn 	callback">
                                                <input type="submit" name="sfReg" value="Сохранить изменения" style="width:250px; color: #000">
                                                <p class="remarks">* поля, обязательные для заполнения</p>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
