@extends('layouts.app')
@section('content')
    <section class="authorize">
        <div class="wrapper">
            <hr>
            <p class="breadcrumbs">
                <a href="{{ route('page.main') }}">Главная</a> / Личный кабинет
            </p>
            <div id="casing" class="module_index">
                <div id="content">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="clr" style="padding: 15px 0"></div>

                        <h1 class="white-h1-left">МОИ ПОДПИСКИ</h1>
                        <br><br>
                        @foreach($subscriptions as $subscription)
                            <div>
                                <form action="/netcat/message.php" method="post">
                                    <div>
                                        <div class="order_header">
                                        </div>
                                        <div class="my-orders">
                                            <div class="col-md-8 col-xs-12 order-data">
                                                <p class="order-name">
                                                    <a href="/my/subscriptions/mysubscription_1023.html">
                                                        {{ $subscription->seminar->course->name }}
                                                    </a>
                                                </p>
                                                <p class="order-city">
                                                    {{ $subscription->seminar->city->name }}
                                                </p>
                                                <p class="order-body">Методист:
                                                    Константин Баев
                                                </p>

                                            </div>
                                            <div class="col-md-4 col-xs-12 schedule-but">
                                                <div class="order-price">
                                                    Дата покупки: 28.04.2019
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endforeach
                        <br><br><br><br>
                        <h1 class="white-h1-left">ДОСТУПНО ДЛЯ ПОДПИСКИ</h1>
                        <br><br>
                        @foreach($possibleSubscriptions as $subscription)
                            <div>
                                <form action="/netcat/message.php" method="post">
                                    <div>
                                        <div class="order_header">
                                        </div>
                                        <div class="my-orders">
                                            <div class="col-md-8 col-xs-12 order-data">
                                                <p class="order-name">
                                                    <a href="/my/subscriptions/mysubscription_1023.html">
                                                        {{ $subscription->seminar->course->name }}
                                                    </a>
                                                </p>
                                                <p class="order-city">
                                                    {{ $subscription->seminar->city->name }}
                                                </p>
                                                <p class="order-body">Методист:
                                                    Константин Баев
                                                </p>

                                            </div>
                                            <div class="col-md-4 col-xs-12 schedule-but">
                                                <div class="order-price">
                                                    Дата покупки: 28.04.2019
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>

    </section>
@endsection