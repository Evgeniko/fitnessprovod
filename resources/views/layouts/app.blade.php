<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" id="root" class="wf-opensans-n4-active wf-active">
<head>
    <meta charset="utf-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Fitness PRO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="keywords" content="Фитнесс Про, Fitness Pro, фитнес тренировка, фитнес тренер, фитнес инструктор, фитнес ижевск">
    <meta name="description" content="Профессиональные фитнес тренировки и мастер классы по всей России">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Roboto|Material+Icons|Roboto+Condensed:300,400,700&amp;subset=cyrillic-ext" rel="stylesheet" type="text/css" media="all">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
{{--    <link type="text/css" rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/base.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/layer7.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/ulightbox.min.css') }}">--}}
    <link type="text/css" rel="stylesheet" href="{{ asset('css/all.css') }}">
    <style type="text/css">
        .UhideBlockL {
            display: none;
        }
    </style>


    <!-- Fonts -->
    {{--<link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">--}}
</head>
<body class="tmpl_body">
<header>
    <div class="wrapper">
        <div id="header">
            <div class="head-t">
                <div class=xs-block>
                    <div class="col-xs-3">
                        <a href="{{ route('page.main') }}"><img src="{{ asset('/images/system/logo-top.png') }}"></a>
                    </div>

                    <div class="col-xs-9">
                        <div class="line1">
                            <div class="nav-head">
                                <div class="material-icons i_menu">
                                    menu
                                </div>
                            </div>
                            <div class="social-buttons">
                                <a href="https://vk.com/fitness__pro" target="_blank"><i class="fa fa-vk" aria-hidden="true"></i></a>
                                <a href="https://www.instagram.com/fitness_pro_russia/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="line1">
                            <div class="head-tel">
                                <a href="tel:+78002010232">8 (800) 201 02 32</a>
                            </div>
                            <div class="login-head">


                                <a href="{{ route('login') }}"><img src="{{ asset('/images/system/logo-top.png') }}"  title="Вход / Регистрация"></a>


                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="line1">
                            <div class="sch-box xs-block">
                                <div class="search-box">
                                    <div class="searchForm">
                                        <form onsubmit="this.sfSbm.disabled=true" method="get" style="margin:0" action="http://www.fitnessprovod.com/search/">
                                            <div align="center" class="schQuery">
                                                <input type="text" name="q" maxlength="50" size="20" class="queryField" placeholder="Поиск по сайту">
                                            </div>
                                            <div align="center" class="schBtn">
                                                <input type="submit" class="searchSbmFl" name="sfSbm" value="Найти">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="not-xs-block">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <a href="{{ route('page.main') }}"><img src="{{ asset('/images/system/logo-top.png') }}"></a>
                    </div>
                    <div class="col-md-9 sol-sm-9 col-xs-9">
                        <div class="line1">
                            <div class="head-tel">
                                <a href="tel:+78002010232">8 (800) 201 02 32</a>
                            </div>
                            <div class="sch-box md-block">
                                <div class="search-box">
                                    <div class="searchForm">
                                        <form onsubmit="this.sfSbm.disabled=true" method="get" style="margin:0" action="http://www.fitnessprovod.com/search/">
                                            <div align="center" class="schQuery">
                                                <input type="text" name="q" maxlength="50" size="20" class="queryField" placeholder="Поиск по сайту">
                                            </div>
                                            <div align="center" class="schBtn">
                                                <input type="submit" class="searchSbmFl" name="sfSbm" value="Найти">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="social-buttons">
                                <a href="https://vk.com/fitness__pro" target="_blank"><i class="fa fa-vk" aria-hidden="true"></i></a>
                                <a href="https://www.instagram.com/fitness_pro_russia/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </div>
                            <div class="login-head">

                                <!-- Authentication Links -->
                                @guest
                                    <a href="{{ route('login') }}"><img src="{{ asset('images/icons/login.png') }}"  title="Вход / Регистрация"></a>
                                @else
                                    <li class="nav-item dropdown">
                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            <img src="{{ asset('images/icons/login.png') }}"/>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('profile.settings') }}">Мой профиль</a>
                                            <a class="dropdown-item" href="{{ route('profile.subscriptions') }}">Мои подписки</a>
                                            <a class="dropdown-item" href="{{ route('profile.purchases') }}">Мои покупки</a>
                                            <a class="dropdown-item" href="{{ route('cart') }}">Корзина</a>
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                Выход
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                        </div>
                                    </li>
                                    {{--<a onclick="show('block')"><img src="{{ asset('images/icons/login.png') }}"  title="Личный кабинет"></a>

                                    <div class="popup" id="popup-lk-mobi">

                                        <ul>
                                            <li><a href="/my/modify/">Мой профиль</a></li>
                                            <li><a href="/my/subscriptions/">Мои подписки</a></li>
                                            <li><a href="/my/orders/">Мои покупки</a></li>
                                        </ul>

                                        <hr>
                                        <a href='#'>Выход</a>

                                    </div>--}}
                                    {{--<li class="nav-item dropdown">
                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    </li>--}}
                                @endguest




                            </div>
                        </div>
                        {{--<script type="text/javascript">
                            function show(state){
                                document.getElementById('popup-lk').style.display = state;
                                document.getElementById('wrap-lk').style.display = state;
                                document.getElementById('popup-lk-mobi').style.display = state;
                                document.getElementById('wrap-lk-mobi').style.display = state;
                            }
                        </script>--}}
                        <div class="line2">
                            <div class="nav-head">
                                <div class="material-icons i_menu">
                                    menu
                                </div>
                            </div>
                            <div class="sch-box sm-block">
                                <div class="search-box">
                                    <div class="searchForm">
                                        <form onsubmit="this.sfSbm.disabled=true" method="get" style="margin:0" action="http://www.fitnessprovod.com/search/">
                                            <div align="center" class="schQuery">
                                                <input type="text" name="q" maxlength="50" size="20" class="queryField" placeholder="Поиск по сайту">
                                            </div>
                                            <div align="center" class="schBtn">
                                                <input type="submit" class="searchSbmFl" name="sfSbm" value="Найти">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <nav>
                                <div id="catmenu">
                                    <div id="uNMenuDiv1" class="uMenuV">
                                        <ul class="uMenuRoot" style="overflow: visible;">

                                            <li><a href='{{ route('seminars') }}'><span>Расписание</span></a></li><li><a href='{{ route('courses') }}'><span>Релизы</span></a></li><li><a href='{{ route('methodists') }}'><span>Методисты</span></a></li><li><a href='{{ route('instructors') }}'><span>Инструкторы</span></a></li><li><a href='{{ route('contacts') }}'><span>Контакты</span></a></li>

                                        </ul>
                                    </div>

                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

@yield('content')

<p style='text-align: center; padding:10px;'>

    <img src="{{ asset('/images/bank/_AlfaBank.png') }}" alt="Фотография 1"  height="40" style='padding-right:10px;'>
    <img src="{{ asset('/images/bank/MIRaccept.png') }}" alt="Фотография 1"  height="40">
    <img src="{{ asset('/images/bank/_Visa.png') }}" alt="Фотография 2"  height="40">
    <img src="{{ asset('/images/bank/_mastercard-securecode.png') }}" alt="Фотография 3"  height="40">


</p>
<footer>
    <div id="footer">
        <div class="wrapper">
            <div class="not-xs-block">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <a href="#"><img src="{{ asset('images/system/logo-bottom.png') }}"></a>
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <div class="col-md-9 col-sm-8 col-xs-9">
                        <div id="botmenu">
                            <div id="uNMenuDiv1" class="uMenuV">
                                <ul class="uMenuRoot" style="overflow: visible;">
                                    <li><a href="privacy/index.html"><span>&shy; Политика конфиденциальности</span></a></li>
                                    <li><a href="dogovor-publichnoy-oferty/index.html" ><span>Договор оферты &shy; </span></a></li>
                                    <li><a href='{{ route('seminars') }}'><span>Расписание</span></a></li><li><a href='{{ route('courses') }}'><span>Релизы</span></a></li><li><a href='{{ route('methodists') }}'><span>Методисты</span></a></li><li><a href='{{ route('instructors') }}'><span>Инструкторы</span></a></li><li><a href='{{ route('contacts') }}'><span>Контакты</span></a></li>


                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-3">
                        <div class="head-tel">
                            <a href="tel:+78002010232">8 (800) 201 02 32</a>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-8 col-xs-9">
                        <p class="copyr">
                            © {{ date('Y') }} FitnessPro. Все права защищены.

                        </p>


                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-3">
                        <div class="social-buttons">
                            <a href="https://vk.com/fitness__pro" target="_blank"><i class="fa fa-vk" aria-hidden="true"></i></a>
                            <a href="https://www.instagram.com/fitness_pro_russia/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </div>
                    </div>

                </div>

            </div>
            <div class="xs-block">
                <div class="col-xs-3">
                    <a href="#"><img src="{{ asset('images/system/logo-bottom.png') }}"></a>
                </div>
                <div class="col-xs-9">
                    <div id="botmenu">
                        <div id="uNMenuDiv1" class="uMenuV">
                            <ul class="uMenuRoot" style="overflow: visible;">

                                <li>
                                    <a href='{{ route('seminars') }}'>
                                        <span>Расписание</span>
                                    </a></li>
                                <li>
                                    <a href='{{ route('courses') }}'>
                                        <span>Релизы</span>
                                    </a>
                                </li>
                                <li>
                                    <a href='{{ route('methodists') }}'>
                                        <span>Методисты</span>
                                    </a>
                                </li>
                                <li><a href='{{ route('instructors') }}'>
                                        <span>Инструкторы</span>
                                    </a>
                                </li>
                                <li>
                                    <a href='{{ route('contacts') }}'>
                                        <span>Контакты</span>
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clr" style="margin-bottom: 5%;"></div>
                <div class="col-xs-6">
                    <div class="head-tel" style="text-align: left">
                        <a href="tel:+78002010232">8 (800) 201 02 32</a>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="social-buttons">
                        <a href="https://vk.com/fitness__pro" target="_blank"><i class="fa fa-vk" aria-hidden="true"></i></a>
                        <a href="https://www.instagram.com/fitness_pro_russia/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="col-xs-12">
                    <p class="copyr">
                        © {{ date('Y') }} FitnessPro. Все права защищены.
                    </p>
                </div>

            </div>

        </div>
    </div>
</footer>

<demomod class="text-center">
    <div>Текущее значение: {{ \App\Models\DemoMod::find(1)->now }}</div>
    <form action="{{ route('change.date') }}" method="post" style="border: solid yellow 10px">
        @csrf
        <label>
            Текущаяя дата:
            <input name="now" type="date">
        </label>
        <button type="submit">Применить</button>
    </form>
</demomod>

<!--/U1BFOOTER1Z-->
<div class="owerflow-layer"></div>
<div class="mm-wrapper">
    <div class="mobile-menu-container">
        <i class="material-icons i_close">close</i>
    </div>
</div>
{{--<script type="text/javascript" src="{{ asset('js/ui.js') }}"></script>
<script src="{{ asset('js/webfont.js') }}"></script>--}}
<!-- Scripts -->

<script src="{{ asset('js/app.js') }}"></script>


<script src="{{ asset('js/all.js') }}"></script>
<script type="text/javascript">
    function show(state){
        document.getElementById('popup-lk').style.display = state;
        document.getElementById('wrap-lk').style.display = state;
        document.getElementById('popup-lk-mobi').style.display = state;
        document.getElementById('wrap-lk-mobi').style.display = state;
    }
</script>


<script type="text/javascript">
    document.ondragstart = noselect;
    document.onselectstart = noselect;
    document.oncontextmenu = noselect;
    function noselect() {return false;}
</script>

<script defer>
    $(function() {
        $('#slider').flexslider({
            animation : "slide",
            slideshowSpeed : 6000,
            controlsContainer : $(".custom-controls-container"),
            customDirectionNav : $(".custom-navigation i")
        });
    })
</script>

</body>
</html>
