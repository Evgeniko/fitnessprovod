@extends('layouts.app')
@section('content')
    <section class="authorize">
        <div class="wrapper" style="text-align: left;">
            <hr>
            <p class="breadcrumbs">
                <a href="{{ route('page.main') }}">Главная</a> / Корзина
            </p>
            <div class="tpl-block-main tpl-component-cart">

                <form method="post" action="{{ route('pay') }}">
                    @csrf
                    <input type="hidden" name="redirect_url" value="/cart/">
                    <input type="hidden" name="cart_mode" value="">

                    <fieldset class="tpl-block-cart-contents">
                        <legend>Корзина</legend>
                        @foreach($products as $product)
                            <div class="tpl-block-cart-item">
                                <!-- Карточка -->
                                <a class="tpl-link-more" href="/raspisanie/raspisanie_219.html">
                                    <!-- Заголовок-->
                                    <h3 class="magenta-h1">
                                        <span class="tpl-property-name" style="color:#fff;">{{ $product->name }}</span>
                                    </h3>

                                    <!-- Подзаголовок -->
                                    <div class="tpl-block-subheader"><span class="tpl-property-type">12 часов</span>, <span class="tpl-property-vendor">Артем Елисеев</span></div>
                                    <!-- Картинка-->
                                    <div class="tpl-property-image"><img src="" width="300px"></div>
                                </a>

                                <br>     <br>
                                <!-- Цена-->
                                <div class="tpl-property-item-price">{{ $product->price }}&nbsp;₽</div>
                                <br>

                                <!-- Удалить-->
                                <button class="tpl-link-cart-remove knopka" title="Удалить из корзины" type="submit" name="cart[488][219]" value="0">
                                    Удалить
                                </button>
                            </div>
                        @endforeach
                        <br>
                        <div class="tpl-block-cart-summary">
                            <div class="tpl-block-cart-totals tpl-property-totals">
                                <span class="tpl-caption">Итого:</span>
                                <span class="tpl-value">{{ $products->sum('price') }}&nbsp;₽</span>
                            </div>
                            <br>
                            <div class="tpl-block-cart-info">

                            </div>
                        </div>
                        <!-- Пересчитать корзину -->
                        <div class="tpl-block-cart-actions">
                            <button type="submit">ОФОРМИТЬ ЗАКАЗ</button>
                            <a class="tpl-link-order-add knopka" href="#" onclick="this.preventDefault();submit();">Оформить заказ</a>
                        </div>
                    </fieldset>
                </form>
                <form method="GET" action="/cart/">
                    <fieldset class="tpl-block-cart-coupons">
                        <legend>Купоны на скидку</legend>
                        <div class="tpl-block-cart-coupons-add" id="content">
                            <div class="schQuery">
                                <input class="queryField" type="text" name="coupon_add" value="">
                            </div>

                            <br> <br>
                            <button type="submit" class="order-button_exit">Применить купон</button>
                            <br> <br>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </section>
@endsection