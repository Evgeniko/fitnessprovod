@extends('layouts.app')
@section('content')
    <div class="wrapper">
        <hr>

        <p class="breadcrumbs">
            <a href="/">Главная</a> / Расписание
        </p>

        <div id="casing" class="module_index">
            <div id="content">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-3 col-sm-12 col-xs-12 schedule-block">
                        <p>Расписание</p>
                    </div>
                    <div class="col-md-4 col-sm-5 col-xs-5 schedule-block1">

                        <select name="f5" class="data-filter" onchange="location='?city='+this.value;">

                            <option selected value="">Все города</option>
                            @foreach($cities as $city)
                                <option value="{{ $city->id }}">{{ $city->name }}</option>
                            @endforeach
                        </select>

                    </div>

                    <div class="col-md-1 col-sm-2 col-xs-2 schedule-block1">
                        <a href="#" onclick="reset()">Сбросить</a>
                    </div>
                    <div class="clr"></div>
                <!-- Карточка-->
                @foreach($seminars as $seminar)
                    <form action="{{ route('cart.add', $seminar->id) }}" method="post">
                        @csrf
                        <input type="hidden" name="redirect_url" value="$_SERVER['REQUEST_URI']"/>
                        <input type="hidden" name="cart_mode" value="add" />
                        <input type="hidden" name="cart-$item['RowID']" value="1" min="0" />
                        <div class="my-orders">
                            <div class="col-md-3 col-sm-3 col-xs-12 order-date">
                                <span class="date-from"><span style="font-size: 36px;font-weight: 400">{{ $seminar->date_start->format('d') }}</span>
                                    {{ $seminar->date_start->format('.m.Y') }}
                                </span>
                                -
                                <span class="date-to"><span style="font-size: 36px;font-weight: 400">{{ $seminar->date_end->format('d') }}</span>
                                    {{ $seminar->date_end->format('.m.Y') }}
                                </span>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 order-data">
                                <p class="order-name"><a href=" $fullLink; " style='font-size:25px;'>{{ $seminar->name }}</a></p>
                                <p class="order-city">{{ $seminar->city->name }}</p>
                                <p class="order-body">Методист:
                                    <a href="{{ route('methodist.view', $seminar->methodist->id) }}">
                                        {{ $seminar->methodist->name }}
                                    </a>
                                </p>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-12 schedule-but">
                                <div class="order-price">
                                    <p class="sch-order-price"> {{ $seminar->price }} ₽</p>
                                    <p class="sch-order-until">{{ $seminar->date_start->format('d.m.Y') }}</p>
                                </div>


                                @guest
                                    <div class='top-5'>
                                        <button class="order-button_exit" id='top-5' type="submit" name="redirect_url" value="/my/modify/"><p>Для оплаты авторизуйтесь!</p></button>
                                    </div>
                                @else
                                    @hasSeminarInCart($seminar->id)
                                        <div class='top-5'>
                                            <a class="order-button" href="{{ route('cart') }}"><p>Уже в корзине</p></a>
                                        </div>
                                    @else
                                    <!-- добавить в корзину и перейти к оформлению заказа -->
                                        <div class='top-5'>
                                            <button class="order-button" type="submit" name="redirect_url" value=" $add_order_url "><p>Добавить в корзину</p></button>
                                        </div>
                                    @endhasSeminarInCart
                                @endguest
                            </div>
                        </div>

                    </form><!-- /RecordTemplate -->
                @endforeach
                    <!-- FormSuffix -->
                </div>
            </div>
        </div>
    </div>
@endsection