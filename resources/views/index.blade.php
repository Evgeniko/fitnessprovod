@extends('layouts.app')
@section('content')
    <div class="wrapper">
        <section>
            <div class="col-md-9 col-sm-12 col-xs-12" style="float:right">
                <div id="slider">



                    <style>

                        @media screen and (max-width: 800px) {


                            .custom-navigation {
                                visibility: hidden;
                                display: none;

                            }



                        }

                        @media screen and (min-width: 700px) {
                            .off-img {
                                visibility: hidden;
                                display: none;


                            }
                        }

                    </style>








                    <ul class="slides">
                        <li class="slide">
                            <g class='on-img'><img   src="{{ asset('images/mainpage/1.png') }}" alt=""></g>
                            <!----g class='off-img'><img   src="" alt=""></g----->
                        </li>


                        <li class="slide">
                            <g class='on-img'><img   src="{{ asset('images/mainpage/2.png') }}" alt=""></g>
                            <!----g class='off-img'><img   src="/netcat_files/5431_30.png" alt=""></g----->
                        </li>

                    </ul>




                </div>
                <div class="custom-navigation">
                    <div class="custom-controls-container"  style='padding-top:60px;'>
                        <i class="material-icons flex-prev">keyboard_arrow_left</i>
                        <i class="material-icons flex-next">keyboard_arrow_right</i>
                    </div>
                </div>


            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 anno">
                <div class="col-md-12 col-sm-3 col-xs-3">
                    <p class="anno-caption">
                        5 лет
                    </p>
                    <p class="anno-text">
                        на рынке фитнес-индустрии
                    </p>
                </div>
                <div class="col-md-12 col-sm-3 col-xs-3">
                    <p class="anno-caption">
                        >20
                    </p>
                    <p class="anno-text">
                        мероприятий в год (фитнес-фестивали, семинары, фитнес-дни, базовые обучающие курсы)
                    </p>
                </div>
                <div class="col-md-12 col-sm-3 col-xs-3">
                    <p class="anno-caption">
                        СОТНИ
                    </p>
                    <p class="anno-text">
                        довольных студентов в разных городах России
                    </p>
                </div>
                <div class="col-md-12 col-sm-3 col-xs-3">

                    <p class="anno-caption">
                        ВСТУПАЙ
                    </p>
                    <p class="anno-text">
                        в ряды профессионалов
                    </p>


                </div>
            </div>
        </section>
    </div>

    <!--Тело-->

    <section class="release">
        <div class="wrapper">
            <h1>НАШИ РЕЛИЗЫ</h1>






            <div class="col-md-4 col-sm-6 col-xs-12">

                <div class="rel-logo">
                    <img src="{{ asset('images/system/5388_4.png') }}">
                </div>

                    <iframe src="https://player.vimeo.com/video/342929663" width="640" height="300" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>


            </div>





            <div class="col-md-4 col-sm-6 col-xs-12">

                <div class="rel-logo">
                    <img src="{{ asset('images/system/5388_3.png') }}">
                </div>
                <div class="rel-video">
                    <script type="text/javascript" src="{{ asset('js/player.js') }}"></script>
                    <script class="splayer"> var params = {"playlist":[{"video":[{"url":"/netcat_files/5387_3.mp4"}],"duration":0,"posterUrl":"/netcat_files/5386_3.png"}],"uiLanguage":"ru","width":"100%","height":"300"}; player.embed(params); </script>

                </div>

            </div>





            <div class="col-md-4 col-sm-6 col-xs-12">

                <div class="rel-logo">
                    <img src="{{ asset('images/system/5388_1.png') }}">
                </div>
                <iframe src="https://player.vimeo.com/video/342929645" width="640" height="300" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>


            </div>








            <div class="col-md-12 col-sm-12 col-xs-12">
                <a href="relizy/index.html">
                    <button class="header1__button">
                        <p>
                            Все релизы
                        </p>
                    </button> </a>
            </div>

        </div>
    </section>

    <section class="schedule">
        <div class="wrapper">
            <div id="casing" class="module_index">
                <div id="content">

                    <section class="sch-top">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <h1 style="margin-top: 0">РАСПИСАНИЕ</h1>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 sch-line">
                            <hr style="padding: 10px 0; margin: 0 0 10px;">
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="raspisanie/index.html">
                                <button class="header1__button">
                                    <p>
                                        Все расписание
                                    </p>
                                </button> </a>
                        </div>
                    </section>

                    <div class="clr"></div>












                    <div class="col-md-3 col-sm-4 col-xs-6 col-xxs">

                        <form action="http://www.fitnessprovod.com/netcat/modules/netshop/actions/cart.php" method="post">

                            <input type="hidden" name="redirect_url" value="/"/>
                            <input type="hidden" name="cart_mode" value="add"/>

                            <input type="hidden" name="cart[488][219]" value="1" min="0" />

                            <!--
                            <input type="hidden" name="items[]" value="488:219"/>
                            <input type="hidden" name="qty" value="1"/>
                            -->

                            <div class="sch-city">
                                <p></p>
                            </div>
                            <div class="sch-desc">
                                <div class="desc-photo">
                                    <a href="raspisanie/raspisanie_219.html">	<img src="#"></a>
                                </div>
                                <div class="desc-about">
                                    <p class="dates">
                                        <span class="date-from"><span style="font-size: 25px"></span>22</span>.06.2019</span> - <span class="date-to"><span style="font-size: 25px"></span>23</span>.06.2019</span>
                                    </p>
                                    <p class="sch-name">Functional VoD</p>
                                    <p class="whois">Артем Елисеев</p>
                                </div>
                                <div class="buyit">
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="padding-left:0">
                                        <p class="sch-price">6500</p>
                                        <p class="sch-until">22</span>.06.2019</p>
                                    </div>
                                    <div class="col-md-7 col-sm-7 col-xs-7" style="padding-right:0">



                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>












                    <div class="col-md-3 col-sm-4 col-xs-6 col-xxs">

                        <form action="http://www.fitnessprovod.com/netcat/modules/netshop/actions/cart.php" method="post">

                            <input type="hidden" name="redirect_url" value="/"/>
                            <input type="hidden" name="cart_mode" value="add"/>

                            <input type="hidden" name="cart[488][232]" value="1" min="0" />

                            <!--
                            <input type="hidden" name="items[]" value="488:232"/>
                            <input type="hidden" name="qty" value="1"/>
                            -->

                            <div class="sch-city">
                                <p></p>
                            </div>
                            <div class="sch-desc">
                                <div class="desc-photo">
                                    <a href="raspisanie/raspisanie_232.html">	<img src="{{ asset('images/system/Stretch_25.png') }}"></a>
                                </div>
                                <div class="desc-about">
                                    <p class="dates">
                                        <span class="date-from"><span style="font-size: 25px"></span>06</span>.07.2019</span> - <span class="date-to"><span style="font-size: 25px"></span>07</span>.07.2019</span>
                                    </p>
                                    <p class="sch-name">Stretching VoD</p>
                                    <p class="whois">Наталья Автаева</p>
                                </div>
                                <div class="buyit">
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="padding-left:0">
                                        <p class="sch-price">6500</p>
                                        <p class="sch-until">06</span>.07.2019</p>
                                    </div>
                                    <div class="col-md-7 col-sm-7 col-xs-7" style="padding-right:0">



                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>












                    <div class="col-md-3 col-sm-4 col-xs-6 col-xxs">

                        <form action="http://www.fitnessprovod.com/netcat/modules/netshop/actions/cart.php" method="post">

                            <input type="hidden" name="redirect_url" value="/"/>
                            <input type="hidden" name="cart_mode" value="add"/>

                            <input type="hidden" name="cart[488][220]" value="1" min="0" />

                            <!--
                            <input type="hidden" name="items[]" value="488:220"/>
                            <input type="hidden" name="qty" value="1"/>
                            -->

                            <div class="sch-city">
                                <p></p>
                            </div>
                            <div class="sch-desc">
                                <div class="desc-photo">
                                    <a href="raspisanie/raspisanie_220.html">	<img src="#"></a>
                                </div>
                                <div class="desc-about">
                                    <p class="dates">
                                        <span class="date-from"><span style="font-size: 25px"></span>06</span>.07.2019</span> - <span class="date-to"><span style="font-size: 25px"></span>07</span>.07.2019</span>
                                    </p>
                                    <p class="sch-name">Functional VoD</p>
                                    <p class="whois">Артем Елисеев</p>
                                </div>
                                <div class="buyit">
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="padding-left:0">
                                        <p class="sch-price">6500</p>
                                        <p class="sch-until">06</span>.07.2019</p>
                                    </div>
                                    <div class="col-md-7 col-sm-7 col-xs-7" style="padding-right:0">



                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>












                    <div class="col-md-3 col-sm-4 col-xs-6 col-xxs">

                        <form action="http://www.fitnessprovod.com/netcat/modules/netshop/actions/cart.php" method="post">

                            <input type="hidden" name="redirect_url" value="/"/>
                            <input type="hidden" name="cart_mode" value="add"/>

                            <input type="hidden" name="cart[488][229]" value="1" min="0" />

                            <!--
                            <input type="hidden" name="items[]" value="488:229"/>
                            <input type="hidden" name="qty" value="1"/>
                            -->

                            <div class="sch-city">
                                <p></p>
                            </div>
                            <div class="sch-desc">
                                <div class="desc-photo">
                                    <a href="raspisanie/raspisanie_229.html">	<img src="{{ asset('images/system/FT_37.png') }}"></a>
                                </div>
                                <div class="desc-about">
                                    <p class="dates">
                                        <span class="date-from"><span style="font-size: 25px"></span>10</span>.08.2019</span> - <span class="date-to"><span style="font-size: 25px"></span>11</span>.08.2019</span>
                                    </p>
                                    <p class="sch-name">Functional VoD</p>
                                    <p class="whois">Валерий Фомин</p>
                                </div>
                                <div class="buyit">
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="padding-left:0">
                                        <p class="sch-price">6500</p>
                                        <p class="sch-until">10</span>.08.2019</p>
                                    </div>
                                    <div class="col-md-7 col-sm-7 col-xs-7" style="padding-right:0">



                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>








                    <script type="text/javascript">
                        function reset() {
                            $('select.data-filter').prop('selectedIndex',0);
                            location='indexd41d.html?';

                        }
                    </script>


                    <!--/Тело-->



















                    <div class="clr" style="margin-bottom: 50px"></div>

                    <section class="sch-top">
                        <div class="col-md-4 col-sm-4 col-xs-12 sch-line">
                            <hr style="padding: 10px 0; margin: 0 0 10px;">
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <a href="raspisanie/index.html">
                                <button class="header1__button">
                                    <p>
                                        Все расписание
                                    </p>
                                </button> </a>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 sch-line">
                            <hr style="padding: 10px 0; margin: 0 0 10px;">
                        </div>
                    </section>

                    <div class="clr"></div>

                    <section class="feedback">
                        <div class="col-md-4 col-sm-12 col-xs-12 col-xxs">
                            <p class="slogan">есть вопросы? Звоните!</p>
                            <p class="sub-slogan">или оставьте заявку и мы вам перезвоним</p>
                        </div>

                        <form method = "post" action = "http://www.fitnessprovod.com/mail.php" onSubmit = "return checkForm (this)">

                            <div class="col-md-3 col-sm-4 col-xs-6 col-xxs">
                                <div align="center" class="schQuery">
                                    <input type="name" name="name" maxlength="50" size="20" class="queryField" placeholder="Имя">
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6 col-xxs">
                                <div align="center" class="schQuery">
                                    <input type="tel" name="tel" id="tel" maxlength="50" size="20" class="queryField" placeholder="Телефон">
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-12 col-xxs">
                                <div align="center" class="schBtn 	callback1">
                                    <input type="submit" name="sfCall" value="Отправить заявку">
                                </div>
                            </div>

                        </form>








                    </section>

                </div>
            </div>
        </div>
    </section>
    <!--/Тело-->
@endsection