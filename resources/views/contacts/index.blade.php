@extends('layouts.app')
@section('content')
<section class="contact">
    <div class="wrapper">
        <hr>
        <p class="breadcrumbs">
            <a href="/">Главная</a> / Контакты
        </p>
        </p>

        <div id="casing" class="module_index">
            <div id="content">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h1 class="white-h1-left">КОНТАКТЫ</h1>
                    <div class="contacts-block">
                        <p class="contact-tel"><i class="fa fa-phone contact-i" aria-hidden="true"></i><a href="tel:+78002010232">8 (800) 201 02 32</a></p>
                        <p class="contact-mail"><i class="fa fa-envelope contact-i" aria-hidden="true"></i><a href="mailto:fitness_pro3@mail.ru">fitness_pro3@mail.ru</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contacts-soc">
        <div class="wrapper">
            <div class="contacts-soc-inner">
                <p>мы в социальных сетях</p>
                <div class="soc-buttons">
                    <a href="https://vk.com/fitness__pro" target="_blank"><i class="fa fa-vk" aria-hidden="true"></i></a>
                    <a href="https://www.instagram.com/fitness_pro_russia/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                </div>
                <p>подписывайся и будь в курсе событий</p>
            </div>
        </div>
    </div>
    <div class="wrapper">
        <div id="casing" class="module_index">
            <div id="content">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="contacts-block">
                        <p class="contact-descr">РЕКВИЗИТЫ<br>ИП Худойкина Светлана Викторовна<br>
                            Адрес: 426000, Удмуртская Республика, г.Ижевск, ул.Ленина, д.13, кв.60<br>
                            ИНН клиента: 183511270214<br>Расчетный счет: 40802810629440000952<br>
                            Название банка: Филиал "НИЖЕГОРОДСКИЙ" АО "АЛЬФА-БАНК"<br>
                            Кор.счет: 30101810200000000824<br>БИК: 042202824<br>
                            тел.: 8 906 816 0812</p>
                    </div>
                </div>
                <section class="feedback" style="margin-bottom:30px">
                    <div class="col-md-4 col-sm-12 col-xs-12 col-xxs">
                        <p class="slogan">есть вопросы? Звоните!</p>
                        <p class="sub-slogan">или оставьте заявку и мы вам перезвоним</p>
                    </div>
                    <form method = "get" action = "/mail.php" onSubmit = "return checkForm (this)">
                        <div class="col-md-3 col-sm-4 col-xs-6 col-xxs">
                            <div align="center" class="schQuery">
                                <input type="text" name="q" maxlength="50" size="20" class="queryField" placeholder="Имя">
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-6 col-xxs">
                            <div align="center" class="schQuery">
                                <input type="text" name="r" id="tel" maxlength="50" size="20" class="queryField" placeholder="Телефон">
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4 col-xs-12 col-xxs">
                            <div align="center" class="schBtn 	callback">
                                <input type="submit" name="sfCall" value="Отправить заявку">
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</section>
@endsection