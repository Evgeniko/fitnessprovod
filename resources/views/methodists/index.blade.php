@extends('layouts.app')
@section('content')
    <section class="authorize">
        <div class="wrapper" style='text-align: left;'>
            <hr>
            <p class="breadcrumbs">
                <a href="{{ route('page.main') }}">Главная</a> / Методисты
            </p>
            @foreach($methodists as $methodist)
                <div class="col-md-4 col-sm-6 col-xs-12 trainer-item">
                    <div class="item">
                        <img src="{{ asset('/images/system/5397_5.png') }}" alt="">
                        <span class="progs-city" style="right:15px">{{ $methodist->city->name }}</span>
                        <a href="{{ route('methodist.view', $methodist->id) }}"><button class="progs-button" style="left:15px">
                                <p>Подробнее</p>
                            </button></a>
                        <div class="desc-about">
                            <p class="trainer-name progs-trainer">{{ $methodist->name }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection