@extends('layouts.app')

@section('content')

    <section class="authorize">
        <div class="wrapper">
            <hr>
            <p class="breadcrumbs">
                <a href="{{ route('page.main') }}">Главная</a> / Личный кабинет
            </p>

            <!----?=nc_browse_level(1, $browse_sub); ?---->
            <div id="casing" class="module_index">
                <div id="content">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="clr" style="padding: 15px 0"></div><div id="casing" class="module_index">
                            <div id="content">


                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <h1 class="white-h1">Войти</h1>
                                    <hr class="hr-title-magenta">
                                    <br/>
                                    <button class="header1__button vk-button vk_login_button" id='login_button_vk' onclick='nc_vk_login()'>
                                        <p><i class="fa fa-vk" aria-hidden="true"></i>&nbsp;&nbsp;Вконтакте</p>
                                    </button>


                                    <div id='fb-root'></div>

                                    <button class="header1__button fb-button" id="fb-auth" onclick="fb_login();">
                                        <p><i class="fa fa-facebook" aria-hidden="true"></i>&nbsp;&nbsp;Facebook</p>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <form method="post" action="{{ route('login') }}" class="tpl-block-auth-form tpl-block-popup" >
                            @csrf
                            <div class="tpl-block-auth-form-title">Вход на сайт</div>
                            <p>
                                <input id="email" name="email" type="email"  placeholder="e-mail" required>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </p>
                            <p>
                                <input id="password" name="password" type="password" placeholder="Пароль" required>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </p>
                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="tpl-block-auth-form-actions">
                                <a href="{{ route('password.request') }}" class="tpl-link-password-recovery">Забыли пароль?</a>
                                <button type="submit">Войти</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>









{{--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
@endsection
