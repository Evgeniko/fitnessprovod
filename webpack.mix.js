const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .styles([
        'public/css/style.css',
        'public/css/base.css',
        'public/css/layer7.css',
        'public/css/ulightbox.min.css',
        'public/css/custom.css'
    ], 'public/css/all.css')
    .scripts([
        'public/js/jquery.flexslider-min.js',
        'public/js/player.js',
        'public/js/ui.js',
        'public/js/ulightbox.min.js',
        'public/js/uwnd.min.js',
        'public/js/webfont.js'
    ], 'public/js/all.js');
