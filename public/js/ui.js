/***Main js***/
window.UcozApp = {};
/**
* Replace all defined symbol in inner HTML of element
*
*@param {string} el .ClassName or #id of element (use identificator)
*@param {object} symbols where  key - the symbol that you want to replace
value - the symbol which should be replaced
*/
window.UcozApp.replaceSymbol = function (el, symbols) {
	"use strict";
	var arr = document.querySelectorAll(el),
		lg = arr.length;
	if (lg) {
		for (var i = 0; i < lg; i++) {
			for (var param in symbols) {
				arr[i].innerHTML = arr[i].innerHTML.replace(param, symbols[param]);
			}
		}
	}
};
/**
 * Add after all defined input[type="checkbox | radio"] <span> with class 'customCheckbox | customRadio'
 * Hide all defined input[type="checkbox | radio"]
 * Copy all event to original input
 */
window.UcozApp.CustomRadio = (function () {
	"use strict";
	var customRadio = {};

	function insertAfter(newNode, referenceNode) {
		referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
	}
    customRadio.init = function () {
        var inputCollection = document.querySelectorAll('input[type="checkbox"], input[type="radio"]');
        for (var i = 0; i < inputCollection.length; i++) {
            if (inputCollection[i].offsetParent !== null && !inputCollection[i].getAttribute('data-parent')) {
                var customInput = document.createElement('span');
                if (inputCollection[i].nextSibling && (inputCollection[i].nextSibling.className === "customRadio" || inputCollection[i].nextSibling.className === "customCheckbox")) {
                    inputCollection[i].parentNode.removeChild(inputCollection[i].nextSibling)
                }
                customInput.className = inputCollection[i].type === 'radio' ? 'customRadio' : 'customCheckbox';
                customInput.title = inputCollection[i].title || '';
                insertAfter(customInput, inputCollection[i]);
                customInput.addEventListener('click', function (event) {
                    event.preventDefault();
                    this.previousSibling.click();
                });
                inputCollection[i].style.display = 'none';
            }
        }
    };
	return customRadio;
})();
UcozApp.replaceSymbol('.catNumData', {
	'[': '(',
	']': ')'
});
UcozApp.replaceSymbol('.pollLnk', {
	'[': '',
	']': '',
	'·': '|'
});
UcozApp.replaceSymbol('.cDate', {
	'(': '',
	')': ''
});
UcozApp.CustomRadio.init();
/*** jQuery Slider Menu Plugin** @version 1.0.0*/
! function (a) {
	"use strict";
	a.fn.sliderMenu = function () {
		a(this).each(function () {
			var b = a(this).clone(),
				c = a("<div>").addClass("slider-menu"),
				d = a("<nav>").addClass("slider-menu__container").attr({
					role: "navigation",
					"aria-label": "Menu"
				}),
				e = 0;
			b.attr("class", "slider-menu__menu"), a("ul", b).addClass("slider-menu__menu").prepend('<li><a href="#" class="slider-menu__back"><span class="slider-menu__text">Back</span></a>').parent().addClass("slider-menu--has-children"), a("li", b).addClass("slider-menu__item"), a("a", b).addClass("slider-menu__link"), d.html(b), a('[data-vertical="true"]', d).addClass("slider-menu__item--vertical"), c.html(d), a(c).on("click", ".slider-menu--has-children .slider-menu__link:not(.slider-menu__back) span", function (b) {
				var c = a(this).parent().attr("href");
				if (c) return location.replace(c), !1
			}), a(c).on("click", ".slider-menu__link", function (b) {
				var c = a(this),
					f = c.closest(".slider-menu"),
					g = c.parent(".slider-menu__item"),
					h = g.parent(".slider-menu__menu"),
					i = a("> .slider-menu__menu", g);
				if (i.length || c.hasClass("slider-menu__back"))
					if (b.preventDefault(), g.data("vertical")) i.is(":visible") ? (h.addClass("slider-menu--active"), i.hide(), f.css("height", h.height()), c.removeClass("slider-menu__link--active-link")) : (i.show(), f.css("height", h.height()), c.addClass("slider-menu__link--active-link"));
					else if (a(".slider-menu__item--vertical .slider-menu__menu", f).hide(), a(".slider-menu__item--vertical .slider-menu__link", f).removeClass("slider-menu__link--active-link"), c.hasClass("slider-menu__back")) {
					var j = h.parent().parent();
					e -= 100, d.css("left", "-" + e + "%"), h.removeClass("slider-menu--active"), j.addClass("slider-menu--active").parents(".slider-menu__menu").addClass("slider-menu--active"), f.css("height", j.height())
				} else e += 100, d.css("left", "-" + e + "%"), h.removeClass("slider-menu--active"), i.addClass("slider-menu--active").parents(".slider-menu__menu").addClass("slider-menu--active"), f.css("height", i.height())
			}), a(this).replaceWith(c)
		})
	}
}(jQuery);
/***Main js***/


var umenu = function(){$('#catmenu').uNavMenu()};

WebFontConfig = {google:{families: ['Open+Sans:400&subset=cyrillic,greek']},active:umenu};

$(function ($) {

	//add class for header if scroll top or bottom
	function hasScrolled() {
		var st = $(this).scrollTop();
		var navbarHeight = $('header').outerHeight();
		if (Math.abs(lastScrollTop - st) <= delta) return;
		if (st > lastScrollTop && st > navbarHeight) {
			$('header').removeClass('nav-down').addClass('nav-up');
		} else {
			if (st + $(window).height() < $(document).height()) {
				$('header').removeClass('nav-up').addClass('nav-down');
			}
		}
		lastScrollTop = st;
	}
	//open menu
	function openMenu() {
		$('.mm-wrapper').addClass('openned');
		$('.owerflow-layer').addClass('openned');
	}
	//close menu
	function closeMenu() {
		$('.mm-wrapper').removeClass('openned');
		$('.owerflow-layer').removeClass('openned');
	}
	//convert text to icon
	function convertToIcon(obj) {
		for (var prop in obj) {
			$(prop).attr('title', $(prop).val()).addClass('material-icons').val(obj[prop]);
		}
	}
	var parentLi = $('#catmenu li.uWithSubmenu');
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var buttonToIcon = {
		'#invoice-form-export': 'file_download',
		'#invoice-form-print': 'print',
		'#invoice-form-send-el-goods': 'forward'
	};
	var captionHight = $('.caption').innerHeight() + 'px';
	$('.mobile-menu-container').append($('#catmenu .uMenuRoot').clone()); //create mobile menu
	$('.mobile-menu-container .uMenuRoot').sliderMenu(); //create slider in mobile menu

	$('> a', parentLi).after('<i class="material-icons menu_tog">keyboard_arrow_down</i>'); //create arrow in nav menu
	$('> i', parentLi).on('click', function () {
		var $this = $(this);
		if ($this.text() == 'keyboard_arrow_down') {
			$this.parent().addClass('over');
			$this.text('keyboard_arrow_up');
		} else {
			$this.parent().removeClass('over');
			$this.text('keyboard_arrow_down');
		}
	});

    window.onresize = umenu;//event after resize
    window.onload = umenu;//event after full load

	$(window).scroll(function (event) {
		didScroll = true;
	});
	setInterval(function () {
		if (didScroll) {
			hasScrolled();
			didScroll = false;
		}
	}, 250);
	$('.nav-head .i_menu').on('click', openMenu);
	$(".owerflow-layer, .i_close").on('click', closeMenu);
	$('.goOnTop').on('click', function (e) {
		e.preventDefault();
		$('body').animate({
			scrollTop: 0
		}, 1000);
	});
	$(document).mouseup(function (e) {
		if ($(".i_person").has(e.target).length === 0 && $("#user-box").has(e.target).length === 0) $('.i_person').removeClass('open');
	});
	$(document).keyup(function (e) {
		if (e.which == 27) {
			closeMenu();
		}
	});
	$('.sidebox ul.cat-tree').removeAttr('style').removeClass('cat-tree');
	var sdLi = $('.sidebox li:has(ul)').addClass('parent-li');
	$(sdLi).each(function () {
		$(this).prepend('<em>+</em>');
	});
	$('> em', sdLi).on('click', function () {
		if ($(this).text() == '+') {
			$(this).parent().addClass('over');
			$(this).text('-');
		} else {
			$(this).parent().removeClass('over');
			$(this).text('+');
		}
	});
	$('.opt_items, .noun, .ui-add-link').on('click', UcozApp.CustomRadio.init);
	$('.shop-spec-filter-wrapper').on('click', '.spec-filter-title, .spec-subfilter-title', UcozApp.CustomRadio.init);
	$('#TstSbm, .pollreSultsBut, [id^="gbut"]').one('click', function () {
		$(document).ajaxComplete(UcozApp.CustomRadio.init);
	});
	$('.slide').css('min-height', captionHight);
	if (typeof shopPageMore === "function") {
		$(document).ajaxComplete(function () {
			UcozApp.CustomRadio.init();
		});
	}
	if ($('#cont-shop-invoices h1').length) {
		$('#cont-shop-invoices h1 + table').addClass('status_table').after('<div class="fil_togg_wrapper"><div class="fil_togg_holder"><span class="material-icons">storage</span><span class="material-icons arrow">keyboard_arrow_down</span></div></div>').siblings('table, hr').addClass('filter_table');
		$('.fil_togg_holder').on('click', function () {
			var arrow = $(this).children('.arrow');
			$('.filter_table').fadeToggle();
			arrow.text(arrow.text() == 'keyboard_arrow_up' ? 'keyboard_arrow_down' : 'keyboard_arrow_up');
		});
		convertToIcon(buttonToIcon);
		$(document).ajaxComplete(function () {
			convertToIcon(buttonToIcon);
		});
	};
    $('.uTable').wrap('<div class="x-scroll"></div>');

});